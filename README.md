# Opencvd NOGC
* Unofficial OpenCV binding for D programming language
* GC-free version of [opencvd](https://github.com/aferust/opencvd)
* Please refer to [opencvd](https://github.com/aferust/opencvd) for building the binding.

## Memory
* Uses [dplug:core](https://github.com/AuburnSounds/Dplug) for Vec!T and manual memory management.
* All return types are slices or vector of slices of malloc-ed memory, and they have to be cleaned up by user such that:

```d
Vec!(Point[]) contours = findContours(bimage, RETR_CCOMP, CHAIN_APPROX_SIMPLE);
Destroy(contours);
...
Vec!(Point2f[]) facets;
Point2f[] centers;

scope(exit){
    Destroy(facets);
    Destroy(centers);
}

subdiv.getVoronoiFacetList(null, facets, centers);

// returning strings has to be freed too!
string ocvver = opencvVersion();

printf("%s \n", ocvver.ptr);

Destroy(ocvver);

```

