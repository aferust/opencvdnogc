import core.stdc.stdio;

import opencvd;
import tesseractd;

/*  text recognition (OCR) with tesseract
    you need this wrapper to run the example: https://github.com/aferust/tesseract-d-wrapper
*/    

int main() @nogc nothrow
{
    TessBaseAPI ocr = TessBaseAPI();
    
    ocr.Init(null, "eng", OEM_DEFAULT); // set your target language here: "eng"
    ocr.SetPageSegMode(PSM_SINGLE_BLOCK);
    
    Mat img = imread("bursa.jpg", IMREAD_COLOR); // read an image that includes some text
    if(img.isEmpty()){
        printf("image not found! \n");
        return -1;
    }

    scope(exit){
        Destroy(ocr);
        Destroy(img);
    }

    namedWindow( "orig", WINDOW_AUTOSIZE );
    imshow("orig", img);
    
    ocr.SetImage(img.ptr, img.cols, img.rows, 3, img.step);
    
    string outText = ocr.GetUTF8Text();
    
    printf("%s \n", outText.ptr);
    
    Destroy(outText);
    
    waitKey();
    return 0;
}
