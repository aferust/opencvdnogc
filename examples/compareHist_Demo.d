import core.stdc.stdio;

import opencvd;

// https://docs.opencv.org/2.4/doc/tutorials/imgproc/histograms/histogram_comparison/histogram_comparison.html

int main( ) @nogc nothrow
{
    Mat src_base = Mat(), hsv_base = Mat();
    Mat src_test1 = Mat(), hsv_test1 = Mat();
    Mat src_test2 = Mat(), hsv_test2 = Mat();
    Mat hsv_half_down = Mat();

    scope(exit){
        Destroy(src_base);
        Destroy(hsv_base);
        Destroy(src_test1);
        Destroy(hsv_test1);
        Destroy(src_test2);
        Destroy(hsv_test2);
        Destroy(hsv_half_down);
    }

    /// Load three images with different environment settings
    src_base = imread( "im1.jpg", 1 );
    src_test1 = imread("im2.jpg", 1 );
    src_test2 = imread("im3.jpg", 1 );

    /// Convert to HSV
    cvtColor( src_base, hsv_base, COLOR_BGR2HSV );
    cvtColor( src_test1, hsv_test1, COLOR_BGR2HSV );
    cvtColor( src_test2, hsv_test2, COLOR_BGR2HSV );

    hsv_half_down = hsv_base[hsv_base.rows/2 .. hsv_base.rows - 1, 0 .. hsv_base.cols - 1]; // multidim slicing

    /// Using 50 bins for hue and 60 for saturation
    int h_bins = 50; int s_bins = 60;
    int[2] histSize = [h_bins, s_bins];

    // hue varies from 0 to 179, saturation from 0 to 255
    float[2] h_ranges = [ 0, 180 ];
    float[2] s_ranges = [ 0, 256 ];

    float[][] ranges = mallocSliceNoInit!(float[])(2);
    scope(exit) Destroy(ranges);
    ranges[0] = h_ranges;
    ranges[1] = s_ranges;

    // Use the o-th and 1-st channels
    int[2] channels = [ 0, 1 ];


    /// Histograms
    Mat hist_base = Mat();
    Mat hist_half_down = Mat();
    Mat hist_test1 = Mat();
    Mat hist_test2 = Mat();

    scope(exit){
        Destroy(hist_base);
        Destroy(hist_half_down);
        Destroy(hist_test1);
        Destroy(hist_test2);
    }

    /// Calculate the histograms for the HSV images
    calcHist( hsv_base, 1, channels, Mat(), hist_base, 2, histSize, ranges, true, false );
    normalize( hist_base, hist_base, double(0), double(1), NORM_MINMAX);

    calcHist( hsv_half_down, 1, channels, Mat(), hist_half_down, 2, histSize, ranges, true, false );
    normalize( hist_half_down, hist_half_down, 0, 1, NORM_MINMAX );

    calcHist( hsv_test1, 1, channels, Mat(), hist_test1, 2, histSize, ranges, true, false );
    normalize( hist_test1, hist_test1, 0, 1, NORM_MINMAX);

    calcHist( hsv_test2, 1, channels, Mat(), hist_test2, 2, histSize, ranges, true, false );
    normalize( hist_test2, hist_test2, 0, 1, NORM_MINMAX );

    string[6] methods;
    methods[0] = "HISTCMP_CORREL";
    methods[1] = "HISTCMP_CHISQR";
    methods[2] = "HISTCMP_INTERSECT";
    methods[3] = "HISTCMP_BHATTACHARYYA/HISTCMP_HELLINGER";
    methods[4] = "HISTCMP_CHISQR_ALT";
    methods[5] = "HISTCMP_KL_DIV";
    
    /// Apply the histogram comparison methods
    foreach(int i, method; methods)
    {
        int compare_method = i;
        double base_base = compareHist( hist_base, hist_base, compare_method );
        double base_half = compareHist( hist_base, hist_half_down, compare_method );
        double base_test1 = compareHist( hist_base, hist_test1, compare_method );
        double base_test2 = compareHist( hist_base, hist_test2, compare_method );

        printf( " Method [%s] Perfect, Base-Half, Base-Test(1), Base-Test(2) : %f, %f, %f, %f \n", method.ptr, base_base, base_half , base_test1, base_test2 );
    }

    printf( "Done \n" );

    return 0;
}