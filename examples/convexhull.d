import opencvd;

// https://docs.opencv.org/4.1.0/d5/d04/samples_2cpp_2convexhull_8cpp-example.html
// tested and working.

int main() @nogc nothrow
{
    Mat img = Mat(500, 500, CV_8UC3);
    
    //setRandomSeed(200);
    
    for(;;)
    {
        int count = rnd!int(0, 100) + 1;
        
        Point[] points = mallocSlice!Point(count);
        scope(exit) Destroy(points);

        foreach(i; 0..count)
        {
            int x = rnd!int(img.cols/4, img.cols*3/4);
            int y = rnd!int(img.rows/4, img.rows*3/4);
            
            points[i] = Point(x, y);
        }
        
        int[] hull = convexHullIdx(points, true);
        scope(exit) Destroy(hull);
        
        img = Scalar.all(0);
        for(int i = 0; i < count; i++ )
            circle(img, points[i], 3, Scalar(0, 0, 255, 255), 1);
        ulong hullcount = hull.length;
        Point pt0 = points[hull[hullcount-1]];
        for( int i = 0; i < hullcount; i++ )
        {
            Point pt = points[hull[i]];
            line(img, pt0, pt, Scalar(0, 255, 0, 255), 1);
            pt0 = pt;
        }
        imshow("hull", img);
        char key = cast(char)waitKey(0);
        if( key == 27 || key == 'q' || key == 'Q' ) // 'ESC'
            break;
    }
    return 0;
}