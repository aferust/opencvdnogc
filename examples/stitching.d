import core.stdc.stdio;

import opencvd;

// based on: https://docs.opencv.org/4.1.0/db/db4/samples_2cpp_2stitching_8cpp-example.html

int main() @nogc nothrow
{
    int mode = Stitcher.PANORAMA;
    
    Mat pan1 = imread("pan1.jpg", IMREAD_COLOR);
    Mat pan2 = imread("pan2.jpg", IMREAD_COLOR);
    
    // Mat[2] imgs = [pan1, pan2];
    // let's do it with vector
    
    Vec!Mat _imgs = makeVec!Mat(2);
    _imgs[0] = pan1;
    _imgs[1] = pan2;
    Mat[] imgs = _imgs[];

    scope(exit) {
        Destroy(pan1);
        Destroy(pan2);
        _imgs.clearContents();
    }
    
    Mat pano = Mat();
    Stitcher stitcher = Stitcher.create(mode);
    scope(exit) Destroy(stitcher);

    int status = stitcher.stitch(imgs, pano);
    
    if (status != Stitcher.OK)
    {
        printf("Can't stitch images, error code : %d", status);
        return -1;
    }
    
    puts("stitching completed successfully\n");
    
    imshow("panorama", pano);
    
    waitKey();
    return 0;
}
