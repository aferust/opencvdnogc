import core.stdc.stdio;

import opencvd;

// idea is from Freddie Honohan

static void applyContrastLimitedAdaptiveHistogramEqualisation(Mat src) @nogc nothrow
{
    auto clahe = CLAHE();
    auto claheCustom = CLAHE(10.0, Size(100,100));
    
    Mat dst = Mat();
    clahe.apply(src, dst);
    imshow("clahe1",dst);

    Mat dst2 = Mat();
    claheCustom.apply(src, dst2);
    imshow("clahe2", dst2);

    Destroy(clahe);
    Destroy(claheCustom);
    Destroy(dst);
    Destroy(dst2);
}

/* denoising function */
static void removeNoise(Mat src) @nogc nothrow
{   
    fastNlMeansDenoising(src, src);
    
    imshow("denoised",src);
}

int main( ) @nogc nothrow
{
    Mat image = imread("sudoku.png", IMREAD_GRAYSCALE);
    
    if(image.empty())
    {
        printf("Cannot read image file");
        return -1;
    }
    
    removeNoise(image);
    applyContrastLimitedAdaptiveHistogramEqualisation(image);
    
    waitKey(0);

    Destroy(image);
    return 0;
} 
