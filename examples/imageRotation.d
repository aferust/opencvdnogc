import core.stdc.stdio;

import opencvd;

// https://www.opencv-srf.com/2010/09/rotating-images.html

int main() @nogc nothrow
{
    // Load the image
    Mat imgOriginal = imread( "lena.jpg", 1 );

    //show the original image
    string pzOriginalImage = "Original Image";
    namedWindow( pzOriginalImage, WINDOW_AUTOSIZE );
    imshow( pzOriginalImage, imgOriginal );

    string pzRotatedImage = "Rotated Image";
    namedWindow( pzRotatedImage, WINDOW_AUTOSIZE );

    int iAngle = 180;
    
    createTrackbar("Angle", pzRotatedImage, &iAngle, 360, null, null);

    int iImageHieght = imgOriginal.rows / 2;
    int iImageWidth = imgOriginal.cols / 2;

    
    while (true)
    {
        Mat matRotation = Mat();
        matRotation = getRotationMatrix2D( Point(iImageWidth, iImageHieght), (iAngle - 180), 1 );
        
        // Rotate the image
        Mat imgRotated = Mat();
        warpAffine( imgOriginal, imgRotated, matRotation, imgOriginal.size() );

        imshow( pzRotatedImage, imgRotated );

        int iRet = waitKey(30);
        if ( iRet == 27 )
        {
            break;
        }

        Destroy(imgRotated);
        Destroy(matRotation);
    }

    return 0;
}
