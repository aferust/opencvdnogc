import core.stdc.stdio;

import opencvd;

// https://docs.opencv.org/4.0.0/dc/dbc/samples_2cpp_2tutorial_code_2HighGUI_2AddingImagesTrackbar_8cpp-example.html 

const int alpha_slider_max = 100;
int alpha_slider;
double alpha;
double beta;

/// Matrices to store images
Mat src1;
Mat src2;
Mat dst;

void on_trackbar( int, void*) @nogc nothrow
{
    
    alpha = cast(double) alpha_slider/alpha_slider_max ;
    beta = ( 1.0 - alpha );
    
    addWeighted( src1, alpha, src2, beta, 0.0, dst);

    imshow( "Linear Blend", dst );
}

int main( ) @nogc nothrow
{
    /// Read image ( same size, same type )
    src1 = imread("dlanglogo.png", 1);
    src2 = imread("ocvlogo.png", 1);
    
    dst = Mat();
    
    scope(exit){
        Destroy(src1);
        Destroy(src2);
        Destroy(dst);
    }

    if( src1.isEmpty ) { printf("Error loading src1 \n"); return -1; }
    if( src2.isEmpty ) { printf("Error loading src2 \n"); return -1; }

    /// Initialize values
    alpha_slider = 0;

    /// Create Windows
    namedWindow("Linear Blend", 1);
    imshow( "Linear Blend", src1 );
    /// Create Trackbars
    
    char[20] buffer;
    sprintf(buffer.ptr, "Alpha x %d", alpha_slider_max);

    string trackbarName = stringIDup(buffer.ptr);

    TrackbarCallback cb = cast(TrackbarCallback)(&on_trackbar);
    
    auto myTb = TrackBar(trackbarName, "Linear Blend", &alpha_slider, alpha_slider_max, cb);

    /// Show some stuff
    on_trackbar( alpha_slider, null );

    Destroy(trackbarName);
    
    /// Wait until user press some key
    waitKey(0);
    return 0;
}
