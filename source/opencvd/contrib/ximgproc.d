module opencvd.contrib.ximgproc;

import std.conv;

import opencvd.cvcore;

private extern (C) @nogc nothrow {
    void Thinning(Mat input, Mat output, int thinningType);
    void FourierDescriptor(Contour src, Mat dst, int nbElt, int nbFD);
    
}

void fourierDescriptor(Point[] src, Mat dst, int nbElt = -1, int nbFD = -1) @nogc nothrow {
    FourierDescriptor(Contour(src.ptr, cast(int)src.length), dst, nbElt, nbFD);
}

enum: int {
    // cv::ximgproc::ThinningTypes
    THINNING_ZHANGSUEN,
    THINNING_GUOHALL
}

void thinning(Mat input, Mat output, int thinningType) @nogc nothrow {
    Thinning(input, output, thinningType);
}
